﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BaseLib.Core.Generator.TESTS
{
	[TestClass]
	public class LazyHelperCoalesceCodeGenerator_Tests {
		private string WrapClassBody(string classBody) {
			var sb = new StringBuilder(classBody);
			WrapClassBody(sb);
			return sb.ToString();
		}
		private void WrapClassBody(StringBuilder classBody) {
			var start =
@"using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs {
	partial class LazyHelper {";

			var end =
@"
	}
}";

			classBody.Insert(0, start);
			classBody.Append(end);
		}

		[TestMethod]
		public void Works_With_One_Type_And_Two_Parameters() {
			var generator = new LazyHelperCoalesceCodeGenerator();
			var config = generator.CreateConfiguration();
			config.InputTypeNames = new string[] { "T" };
			var body = generator.GenerateClassBody(config);
			var result = this.WrapClassBody(body);

			Assert.Inconclusive();
		}

		[TestMethod]
		public void Works_With_Full_Configuration() {
			var generator = new LazyHelperCoalesceCodeGenerator();
			var config = generator.CreateConfiguration();
			config.InputTypeNames = new string[] { "T", "Func<T>", "WeakReference<T>", "Lazy<T>" };
			config.ParametersCount = 4;
			var body = generator.GenerateClassBody(config);
			var result = this.WrapClassBody(body);

			Assert.Inconclusive();
		}
	}
}

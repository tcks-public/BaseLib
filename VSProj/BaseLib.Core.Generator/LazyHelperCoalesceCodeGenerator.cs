﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TcKs;
using TcKs.Collections;

namespace BaseLib.Core.Generator {
	public sealed class LazyHelperCoalesceCodeGenerator {
		public sealed class Configuration {
			public string[] InputTypeNames { get; set; }
			public byte ParametersCount { get; set; }

			public Configuration DeepClone() {
				return new Configuration {
					InputTypeNames = this.InputTypeNames?.ToArray(),
					ParametersCount = this.ParametersCount
				};
			}
		}
		public Configuration CreateConfiguration() => new Configuration { ParametersCount = 2 };

		public string GenerateClassBody(Configuration config) {
			var classBody = new StringBuilder();
			this.GenerateClassBody(classBody, config);
			return classBody.ToString();
		}

		private sealed class Context {
			public readonly StringBuilder Code;
			public readonly Configuration Config;
			public readonly List<string> ParametersTypeNames = new List<string>();

			public Context(StringBuilder code, Configuration config) {
				this.Code = code ?? throw new ArgumentNullException(nameof(code));
				this.Config = config ?? throw new ArgumentNullException(nameof(config));
			}

#if DEBUG
			public int GeneratedMethodCounter;
#endif
		}

		public void GenerateClassBody(StringBuilder classBody, Configuration config) {
			var okConfig = config.DeepClone();
			if (okConfig.ParametersCount < okConfig.InputTypeNames.Length) {
				okConfig.ParametersCount = (byte)okConfig.InputTypeNames.Length;
			}

			var cx = new Context(classBody, okConfig);
			this.GenerateClassBody(cx);
		}

		private void GenerateClassBody(Context context) {
			var remainingParameters = context.Config.ParametersCount - context.ParametersTypeNames.Count;
			if (remainingParameters < 0) {
				// this is not possible
				throw new InvalidOperationException("This can not happen. (remainingParameters < 0)");
			}

			if (0 == remainingParameters) {
				// now we want generate the function
				this.GenerateFinalVariants(context);
				return;
			}

			foreach (var typeName in context.Config.InputTypeNames) {
				// add type on the stack and continue
				context.ParametersTypeNames.Add(typeName);
				this.GenerateClassBody(context);

				// remove type from the stack and go to next type name
				context.ParametersTypeNames.RemoveLast();
			}
		}

		private void GenerateFinalVariants(Context context) {
			const string TAB = "\t";
			const string TAB2 = TAB + TAB;

			var code = context.Code;

			string ParameterDeclaration(string name, int index) => $"{name} {ValueParameterName(index)}";
			var paramDeclarations = string.Join(", ", context.ParametersTypeNames.Select(ParameterDeclaration));
			var coreBody = string.Join($" ?? ", context.ParametersTypeNames.Select((name, index) => EvaluatedArgument(index)));

			code.AppendLine();
#if DEBUG
			context.GeneratedMethodCounter++;
			code.AppendLine($"{TAB2}// DEBUG: {nameof(Context.GeneratedMethodCounter)}: {context.GeneratedMethodCounter}");
#endif
			GenerateCoalesceMethod(TAB2, code, paramDeclarations, coreBody);
		}

		private void GenerateCoalesceMethod(string indentation, StringBuilder code, string paramDeclarations, string coreBody) {
			code.AppendLine($"{indentation}[MethodImpl(MethodImplOptions.AggressiveInlining)]");
			code.Append($"{indentation}public static T Coalesce<T>({paramDeclarations}) where T : class");
			code.AppendLine($" => {coreBody};");
		}

		private string EvaluatedArgument(int index) {
			return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}<T>({1})", nameof(LazyHelper.Coalesce), ValueParameterName(index));
		}

		private string ValueParameterName(int index) {
			return string.Format(System.Globalization.CultureInfo.InvariantCulture, "value{0}", index);
		}
	}
}

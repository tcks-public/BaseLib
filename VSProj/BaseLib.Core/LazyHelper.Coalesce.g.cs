﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs {
	partial class LazyHelper {
		// DEBUG: GeneratedMethodCounter: 1
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 2
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 3
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 5
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 6
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 7
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 8
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 9
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 10
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 11
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 12
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 13
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 14
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 15
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 16
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 17
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 18
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 19
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 20
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 21
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 22
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 23
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 24
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 25
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 26
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 27
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 28
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 29
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 30
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 31
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 32
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 33
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 34
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 35
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 36
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 37
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 38
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 39
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 40
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 41
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 42
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 43
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 44
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 45
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 46
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 47
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 48
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 49
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 50
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 51
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 52
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 53
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 54
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 55
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 56
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 57
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 58
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 59
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 60
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 61
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 62
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 63
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 64
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Lazy<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 65
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 66
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 67
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 68
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 69
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 70
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 71
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 72
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 73
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 74
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 75
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 76
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 77
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 78
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 79
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 80
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 81
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 82
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 83
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 84
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 85
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 86
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 87
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 88
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 89
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 90
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 91
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 92
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 93
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 94
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 95
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 96
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 97
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 98
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 99
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 100
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 101
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 102
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 103
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 104
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 105
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 106
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 107
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 108
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 109
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 110
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 111
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 112
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 113
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 114
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 115
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 116
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 117
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 118
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 119
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 120
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 121
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 122
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 123
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 124
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 125
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 126
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 127
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 128
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Lazy<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 129
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 130
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 131
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 132
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 133
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 134
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 135
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 136
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 137
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 138
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 139
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 140
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 141
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 142
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 143
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 144
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 145
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 146
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 147
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 148
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 149
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 150
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 151
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 152
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 153
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 154
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 155
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 156
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 157
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 158
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 159
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 160
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 161
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 162
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 163
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 164
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 165
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 166
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 167
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 168
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 169
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 170
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 171
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 172
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 173
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 174
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 175
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 176
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 177
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 178
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 179
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 180
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 181
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 182
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 183
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 184
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 185
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 186
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 187
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 188
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 189
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 190
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 191
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 192
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Lazy<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 193
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 194
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 195
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 196
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 197
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 198
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 199
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 200
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 201
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 202
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 203
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 204
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 205
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 206
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 207
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 208
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, T value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 209
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 210
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 211
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 212
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 213
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 214
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 215
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 216
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 217
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 218
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 219
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 220
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 221
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 222
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 223
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 224
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Func<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 225
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 226
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 227
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 228
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 229
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 230
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 231
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 232
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 233
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 234
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 235
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 236
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 237
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 238
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 239
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 240
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, WeakReference<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 241
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, T value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 242
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, T value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 243
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, T value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 244
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, T value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 245
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Func<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 246
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Func<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 247
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Func<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 248
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Func<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 249
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, WeakReference<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 250
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, WeakReference<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 251
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, WeakReference<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 252
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, WeakReference<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 253
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Lazy<T> value2, T value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 254
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Lazy<T> value2, Func<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 255
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Lazy<T> value2, WeakReference<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

		// DEBUG: GeneratedMethodCounter: 256
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Lazy<T> value0, Lazy<T> value1, Lazy<T> value2, Lazy<T> value3) where T : class => Coalesce<T>(value0) ?? Coalesce<T>(value1) ?? Coalesce<T>(value2) ?? Coalesce<T>(value3);

	}
}
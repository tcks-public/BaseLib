﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs {
	public static class WeakReferenceExtensions {
		/// <summary>
		/// Returns target of weak reference.
		/// </summary>
		/// <typeparam name="T">Type of contained reference.</typeparam>
		/// <param name="self">Container of reference.</param>
		/// <returns></returns>
		public static T PeekTarget<T>(this WeakReference<T> self) where T : class {
			self.TryGetTarget(out var result);
			return result;
		}
	}
}

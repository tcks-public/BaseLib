﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.Collections {
	public static class ListExtensions {
		public static int RemoveLast<T>(this IList<T> self) {
			var cnt = self.Count;
			if (cnt < 1) { return -1; }

			var ndx = cnt - 1;
			self.RemoveAt(ndx);
			return ndx;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs {
	partial class LazyHelper {
		#region Scalar values
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0) where T : class => value0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0) where T : class => value0?.Invoke();

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0) where T : class => value0?.PeekTarget();

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Lazey<T>(Lazy<T> value0) where T : class => value0?.Value;
		#endregion Scalar values

		#region Arrays
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params T[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var item = values[i];
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params Func<T>[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var func = values[i];
				if (func is null) { continue; }

				var item = func();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params WeakReference<T>[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var reference = values[i];
				if (reference is null) { continue; }

				var item = reference.PeekTarget();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params Lazy<T>[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var reference = values[i];
				if (reference is null) { continue; }

				var item = reference.Value;
				if (item is null) { continue; }

				return item;
			}

			return null;
		}
		#endregion Arrays

		#region Enumerables
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<T> values) where T : class {
			if (values is null) { return null; }

			foreach (var item in values) {
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<Func<T>> values) where T : class {
			if (values is null) { return null; }

			foreach (var func in values) {
				if (func is null) { continue; }
				var item = func();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<WeakReference<T>> values) where T : class {
			if (values is null) { return null; }

			foreach (var reference in values) {
				if (reference is null) { continue; }
				var item = reference.PeekTarget();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<Lazy<T>> values) where T : class {
			if (values is null) { return null; }

			foreach (var reference in values) {
				if (reference is null) { continue; }
				var item = reference.Value;
				if (item is null) { continue; }

				return item;
			}

			return null;
		}
		#endregion Enumerables
	}
}

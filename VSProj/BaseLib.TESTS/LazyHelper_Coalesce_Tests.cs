﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.BaseLib {
	[TestClass]
	public class LazyHelper_Coalesce_Tests {
		[TestMethod]
		public void Works_Without_Provider() {
			object field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce(ref field, null);
			Assert.AreSame(rslt_0, field);
			Assert.IsNull(rslt_0);

			var obj_A = new object();
			field = obj_A;
			var rslt_1 = LazyHelper.PersistedCoalesce(ref field, null);
			Assert.AreSame(rslt_1, field);
			Assert.AreSame(rslt_1, obj_A);

			var rslt_2 = LazyHelper.PersistedCoalesce(ref field, null);
			Assert.AreSame(rslt_1, rslt_2);
			Assert.AreSame(obj_A, field);
		}

		[TestMethod]
		public void Works_With_Provider_Returning_Null() {
			object field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce(ref field, () => null);
			Assert.AreSame(rslt_0, field);
			Assert.IsNull(rslt_0);

			var obj_A = new object();
			field = obj_A;
			var rslt_1 = LazyHelper.PersistedCoalesce(ref field, () => null);
			Assert.AreSame(rslt_1, field);
			Assert.AreSame(rslt_1, obj_A);

			var rslt_2 = LazyHelper.PersistedCoalesce(ref field, () => null);
			Assert.AreSame(rslt_1, rslt_2);
			Assert.AreSame(obj_A, field);
		}

		[TestMethod]
		public void Works_With_Provider_Returning_Instance() {
			var obj_A = new object();
			var obj_B = new object();

			object field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce(ref field, () => obj_A);
			Assert.AreSame(rslt_0, obj_A);
			Assert.AreSame(field, obj_A);

			var rslt_1 = LazyHelper.PersistedCoalesce(ref field, () => obj_B);
			Assert.AreSame(rslt_1, obj_A);
			Assert.AreSame(field, obj_A);

			field = null;
			var rslt_2 = LazyHelper.PersistedCoalesce(ref field, () => obj_B);
			Assert.AreSame(rslt_2, obj_B);
			Assert.AreSame(field, obj_B);
		}

		[TestMethod]
		public void Works_With_WeakReference_Without_Provider() {
			WeakReference<object> field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce<object>(ref field, null);
			Assert.IsNull(field);
			Assert.IsNull(rslt_0);

			var origField = field = new WeakReference<object>(null);
			var rslt_1 = LazyHelper.PersistedCoalesce<object>(ref field, null);
			Assert.AreSame(origField, field);
			Assert.IsNull(rslt_1);

			var obj_A = new object();
			field.SetTarget(obj_A);
			var rslt_2 = LazyHelper.PersistedCoalesce<object>(ref field, null);
			Assert.AreSame(origField, field);
			Assert.AreSame(obj_A, rslt_2);
		}

		[TestMethod]
		public void Works_With_WeakReference_With_Provider_Returning_Null() {
			WeakReference<object> field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce<object>(ref field, () => null);
			Assert.IsNull(field);
			Assert.IsNull(rslt_0);

			var origField = field = new WeakReference<object>(null);
			var rslt_1 = LazyHelper.PersistedCoalesce<object>(ref field, () => null);
			Assert.AreSame(origField, field);
			Assert.IsNull(rslt_1);

			var obj_A = new object();
			field.SetTarget(obj_A);
			var rslt_2 = LazyHelper.PersistedCoalesce<object>(ref field, () => null);
			Assert.AreSame(origField, field);
			Assert.AreSame(obj_A, rslt_2);
		}

		[TestMethod]
		public void Works_With_WeakReference_With_Provider_Returning_Instance() {
			var obj_A = new object();
			var obj_B = new object();
			var obj_C = new object();

			WeakReference<object> field = null;
			var rslt_0 = LazyHelper.PersistedCoalesce(ref field, () => obj_A);
			Assert.AreSame(rslt_0, obj_A);
			Assert.IsNotNull(field);
			var origField = field;

			var rslt_1 = LazyHelper.PersistedCoalesce(ref field, () => obj_B);
			Assert.AreSame(rslt_1, obj_A);
			Assert.AreSame(origField, field);

			field.SetTarget(null);
			var rslt_2 = LazyHelper.PersistedCoalesce(ref field, () => obj_C);
			Assert.AreSame(rslt_2, obj_C);
			Assert.AreSame(origField, field);
		}
	}
}

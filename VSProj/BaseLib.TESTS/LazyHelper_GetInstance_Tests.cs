﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.BaseLib {
	[TestClass]
	public class LazyHelper_GetInstance_Tests {
		[TestMethod]
		public void Works() {
			object field = null;
			var rslt_0 = LazyHelper.GetInstance(ref field);
			Assert.AreSame(rslt_0, field);
			Assert.IsNotNull(rslt_0);

			var rslt_1 = LazyHelper.GetInstance(ref field);
			Assert.AreSame(rslt_0, rslt_1);
		}

		[TestMethod]
		public void Works_With_WeakReference() {
			WeakReference<object> field = null;

			var rslt_0 = LazyHelper.GetInstance(ref field);
			Assert.IsNotNull(field);
			Assert.AreSame(rslt_0, field.PeekTarget());

			var origField = field;
			var rslt_1 = LazyHelper.GetInstance(ref field);
			Assert.AreSame(origField, field);
			Assert.AreSame(rslt_0, rslt_1);
			Assert.AreSame(rslt_1, field.PeekTarget());

			field.SetTarget(null);
			var rslt_2 = LazyHelper.GetInstance(ref field);
			Assert.AreSame(origField, field);
			Assert.AreNotSame(rslt_2, rslt_1);
			Assert.AreSame(rslt_2, field.PeekTarget());
		}
	}
}

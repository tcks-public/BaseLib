﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.BaseLib
{
	[TestClass]
	public class WeakReferenceExtensions_Tests {
		[TestMethod]
		public void PeekTarget_Works() {
			var wref = new WeakReference<object>(null);
			Assert.IsNull(wref.PeekTarget());
			Assert.IsNull(wref.PeekTarget());

			var obj_A = new object();
			wref.SetTarget(obj_A);
			Assert.AreSame(obj_A, wref.PeekTarget());
			Assert.AreSame(obj_A, wref.PeekTarget());

			var obj_B = new object();
			wref.SetTarget(obj_B);
			Assert.AreSame(obj_B, wref.PeekTarget());
			Assert.AreSame(obj_B, wref.PeekTarget());
		}
	}
}

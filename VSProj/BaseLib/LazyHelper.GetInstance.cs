﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		/// <summary>
		/// Returns instance (not-null) of <typeparamref name="T"/> from <paramref name="field"/>.
		/// If <paramref name="field"/> is null, new instance of <typeparamref name="T"/> is created and assigned to <paramref name="field"/>.
		/// </summary>
		/// <typeparam name="T">Type of instance.</typeparam>
		/// <param name="field">Referenced field with instance.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T GetInstance<T>(ref T field) where T : class, new() {
			var result = field;
			if (result is null) {
				field = result = new T();
			}

			return result;
		}

		/// <summary>
		/// Returns instance (not-null) of <typeparamref name="T"/> from <paramref name="field"/>.
		/// If <paramref name="field"/> is null or contains null, new instance of <typeparamref name="T"/> is created and assigned to <paramref name="field"/>.
		/// </summary>
		/// <typeparam name="T">Type of instance.</typeparam>
		/// <param name="field">Referenced field with instance.</param>
		/// <returns></returns>
		public static T GetInstance<T>(ref WeakReference<T> field) where T : class, new() {
			T result;

			var wref = field;
			if (wref is null) {
				result = new T();
				field = new WeakReference<T>(result);
			}
			else {
				wref.TryGetTarget(out result);
				if (result is null) {
					result = new T();
					wref.SetTarget(result);
				}
			}
			return result;
		}
	}
}

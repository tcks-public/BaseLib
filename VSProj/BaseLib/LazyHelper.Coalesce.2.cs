﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		#region Level T
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, params T[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, params Func<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, params WeakReference<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);
		#endregion Level T

		#region Level Func<T>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, T value1) where T : class => Coalesce(value0) ?? value1;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, params T[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, Func<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, params Func<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, WeakReference<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0, params WeakReference<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);
		#endregion Level Fun<T>

		#region Level WeakReference<T>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, T value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, params T[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, Func<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, params Func<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, WeakReference<T> value1) where T : class => Coalesce(value0) ?? Coalesce(value1);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0, params WeakReference<T>[] values) where T : class => Coalesce(value0) ?? Coalesce(values);
		#endregion Level WeakReference<T>
	}
}

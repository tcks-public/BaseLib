﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, T defaultValue0, T defaultValue1, T defaultValue2) where T : class {
			return value ?? defaultValue0 ?? defaultValue1 ?? defaultValue2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, Func<T> defaultValueProvider0, T defaultValue1, T defaultValue2) where T : class {
			return value ?? defaultValueProvider0?.Invoke() ?? defaultValue1 ?? defaultValue2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, T defaultValue0, Func<T> defaultValueProvider1, T defaultValue2) where T : class {
			return value ?? defaultValue0 ?? defaultValueProvider1?.Invoke() ?? defaultValue2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, T defaultValue0, T defaultValue1, Func<T> defaultValueProvider2) where T : class {
			return value ?? defaultValue0 ?? defaultValue1 ?? defaultValueProvider2?.Invoke();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, Func<T> defaultValueProvider0, Func<T> defaultValueProvider1, T defaultValue2) where T : class {
			return value ?? defaultValueProvider0?.Invoke() ?? defaultValueProvider1?.Invoke() ?? defaultValue2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, Func<T> defaultValueProvider0, T defaultValue1, Func<T> defaultValueProvider2) where T : class {
			return value ?? defaultValueProvider0?.Invoke() ?? defaultValue1 ?? defaultValueProvider2?.Invoke();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, T defaultValue0, Func<T> defaultValueProvider1, Func<T> defaultValueProvider2) where T : class {
			return value ?? defaultValue0 ?? defaultValueProvider1?.Invoke() ?? defaultValueProvider2?.Invoke();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value, Func<T> defaultValueProvider0, Func<T> defaultValueProvider1, Func<T> defaultValueProvider2) where T : class {
			return value ?? defaultValueProvider0?.Invoke() ?? defaultValueProvider1?.Invoke() ?? defaultValueProvider2?.Invoke();
		}
	}
}

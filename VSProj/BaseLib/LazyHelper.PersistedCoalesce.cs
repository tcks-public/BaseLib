﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		/// <summary>
		/// Returns instance of <typeparamref name="T"/> from <paramref name="field"/>.
		/// If <paramref name="field"/> is null, the <paramref name="defaultValue"/> is returned.
		/// If <paramref name="field"/> was null and result is not null, the result is assigned to <paramref name="field"/>.
		/// </summary>
		/// <typeparam name="T">Type of instance.</typeparam>
		/// <param name="field">Referenced field with instance.</param>
		/// <param name="defaultValue">The value used if <paramref name="field"/> is null.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T PersistedCoalesce<T>(ref T field, T defaultValue) where T : class {
			var result = field;
			if (result is null && !(defaultValue is null)) {
				field = result = defaultValue;
			}

			return result;
		}

		/// <summary>
		/// Returns instance of <typeparamref name="T"/> from <paramref name="field"/>.
		/// If <paramref name="field"/> is null, the <paramref name="provider"/> is called and (not-null) result assigned to <paramref name="field"/>.
		/// If <paramref name="provider"/> is null or returns null, nothing is assigned to <paramref name="field"/>.
		/// </summary>
		/// <typeparam name="T">Type of instance.</typeparam>
		/// <param name="field">Referenced field with instance.</param>
		/// <param name="provider">Function providing instance if <paramref name="field"/> is null. Ignored if is null.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T PersistedCoalesce<T>(ref T field, Func<T> provider) where T : class {
			var result = field;
			if (result is null && !(provider is null)) {
				result = provider();
				if (!(result is null)) {
					field = result;
				}
			}

			return result;
		}

		/// <summary>
		/// Returns instance of <typeparamref name="T"/> from <paramref name="field"/>.
		/// If <paramref name="field"/> is null or contains null, the <paramref name="provider"/> is called and (not-null) result assigned to <paramref name="field"/>.
		/// If <paramref name="provider"/> is null or returns null, nothing is assigned to <paramref name="field"/>.
		/// </summary>
		/// <typeparam name="T">Type of instance.</typeparam>
		/// <param name="field">Referenced field with instance.</param>
		/// <param name="provider">Function providing instance if <paramref name="field"/> is null. Ignored if is null.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T PersistedCoalesce<T>(ref WeakReference<T> field, Func<T> provider) where T : class {
			T result;

			var wref = field;
			if (wref is null) {
				if (provider is null) {
					result = null;
				}
				else {
					result = provider();
					if (!(result is null)) {
						field = new WeakReference<T>(result);
					}
				}
			}
			else {
				wref.TryGetTarget(out result);
				if (result is null) {
					if (provider is null) {
						result = null;
					}
					else {
						result = provider();
						if (!(result is null)) {
							wref.SetTarget(result);
						}
					}
				}
			}

			return result;
		}
	}
}

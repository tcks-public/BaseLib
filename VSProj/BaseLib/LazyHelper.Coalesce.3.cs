﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		#region Level T, T
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, T value2) where T : class {
			return value0 ?? value1 ?? value2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, params T[] values) where T : class {
			return value0 ?? value1 ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, Func<T> value2) where T : class {
			return value0 ?? value1 ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, params Func<T>[] values) where T : class {
			return value0 ?? value1 ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, WeakReference<T> value2) where T : class {
			return value0 ?? value1 ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, T value1, params WeakReference<T>[] values) where T : class {
			return value0 ?? value1 ?? Coalesce(values);
		}
		#endregion Level T, T

		#region Level T, Func<T>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, T value2) where T : class {
			return value0 ?? Coalesce(value1) ?? value2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, params T[] values) where T : class {
			return value0 ?? Coalesce(value1) ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, Func<T> value2) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, params Func<T>[] values) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, WeakReference<T> value2) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, Func<T> value1, params WeakReference<T>[] values) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(values);
		}
		#endregion Level T, Func<T>

		#region Level T, WeakReference<T>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, T value2) where T : class {
			return value0 ?? Coalesce(value1) ?? value2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, params T[] values) where T : class {
			return value0 ?? Coalesce(value1) ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, Func<T> value2) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, params Func<T>[] values) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(values);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, WeakReference<T> value2) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(value2);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0, WeakReference<T> value1, params WeakReference<T>[] values) where T : class {
			return Coalesce(value0) ?? Coalesce(value1) ?? Coalesce(values);
		}
		#endregion Level T, WeakReference<T>
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(T value0) where T : class => value0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(Func<T> value0) where T : class => value0?.Invoke();

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(WeakReference<T> value0) where T : class => value0?.PeekTarget();
	}
}

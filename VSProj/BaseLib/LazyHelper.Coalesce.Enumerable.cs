﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<T> values) where T : class {
			if (values is null) { return null; }

			foreach (var item in values) {
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<Func<T>> values) where T : class {
			if (values is null) { return null; }

			foreach (var func in values) {
				if (func is null) { continue; }
				var item = func();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(IEnumerable<WeakReference<T>> values) where T : class {
			if (values is null) { return null; }

			foreach (var reference in values) {
				if (reference is null) { continue; }
				var item = reference.PeekTarget();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}
	}
}

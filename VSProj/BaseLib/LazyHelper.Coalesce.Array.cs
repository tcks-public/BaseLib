﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TcKs.BaseLib {
	partial class LazyHelper {
		#region Level Array
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params T[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var item = values[i];
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params Func<T>[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var func = values[i];
				if (func is null) { continue; }

				var item = func();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T Coalesce<T>(params WeakReference<T>[] values) where T : class {
			if (values is null) { return null; }

			for (var i = 0; i < values.Length; i++) {
				var reference = values[i];
				if (reference is null) { continue; }

				var item = reference.PeekTarget();
				if (item is null) { continue; }

				return item;
			}

			return null;
		}
		#endregion Level Array
	}
}
